#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //QPixmap openEyeImage("image/eye.png");
    //QPixmap closedEyeImage("image/closedEye.png");
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

}

MainWindow::~MainWindow()
{
    delete ui;
}

QString filePath; // global variable for database path

void MainWindow::on_pushButton_6_clicked() // browse
{
    filePath = QFileDialog::getOpenFileName(this, "Select your file", "/");

    ui->label_3->setText(filePath);

}

void MainWindow::on_actionAdd_new_triggered()
{
    int myInteger = 0;
    QString extention = ".db";

    db = QSqlDatabase::addDatabase("QSQLITE");

    for(;myInteger<1000;myInteger++){
        QString nameGenerator = "./Passwords" + QString::number(myInteger);
        nameGenerator.append(extention);
        try {
            if(!std::filesystem::exists(nameGenerator.toStdString())){
                db.setDatabaseName(nameGenerator);
                qDebug() << QString("database created %1").arg(nameGenerator);
                break;
            }
        } catch (...) {
            qDebug() << QString("name taken %1").arg(nameGenerator);
            continue;
        }
    }


    if(db.open()){
        qDebug("open");
        ui->stackedWidget->setCurrentIndex(1);
    }else{
        qDebug("no open");
    }

    query = new QSqlQuery(db);
    query->exec("CREATE TABLE DataBook(Email TEXT, Username TEXT, Password TEXT);");

    model = new QSqlTableModel(this, db);
    model->setTable("DataBook");
    model->select();

    ui->tableView->setModel(model);

}


void MainWindow::on_actionExit_triggered()
{
    QApplication::quit();
}


void MainWindow::on_actionInfo_triggered()
{
    QMessageBox::information(this,"Information", "SecurePasswdX\nversion 1.0\nAuthor: Ernest Ivanov", QMessageBox::Ok);
}


void MainWindow::on_pushButton_7_clicked() // add button
{
    model->insertRow(model->rowCount());
}


void MainWindow::on_pushButton_5_clicked() // remove button
{
    model->removeRow(row);
}


void MainWindow::on_tableView_clicked(const QModelIndex &index)
{
    row = index.row();
}


void MainWindow::on_pushButton_3_clicked()
{
    QApplication::quit();
}


void MainWindow::on_pushButton_2_clicked()
{
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(filePath);
    if(db.open()){
        qDebug("open");
        ui->stackedWidget->setCurrentIndex(1);
    }else{
        qDebug("no open");
    }

    query = new QSqlQuery(db);
    query->exec("CREATE TABLE DataBook(Email TEXT, Username TEXT, Password TEXT);");

    model = new QSqlTableModel(this, db);
    model->setTable("DataBook");
    model->select();

    ui->tableView->setModel(model);
}


void MainWindow::on_actionLock_database_triggered()
{
    db.close();
    ui->stackedWidget->setCurrentIndex(0);
}

