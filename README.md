# SecurePasswdX

<a href='#'>
    <img src='https://img.shields.io/badge/build-passing-succes.svg' alt='build-passing' />
</a>
<br>
<br>

<h3>[!] THIS IS PRODUCTION VERSION [!]<br> NO ENCRYPTION</h3>
<h3>[!] Change branch to Beta for using encryption [!]</h3>

<b>SecurePasswdX</b> - simple password manager that has strong encryption.

<img src="img/password-manager.png">